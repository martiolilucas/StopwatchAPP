﻿using System;

namespace StopwatchAPP
{
    public class StopWatch
    {

        public TimeSpan TimeSpan { set; get; }
        private DateTime StartTime { set; get; }
        private bool Started { set; get; }

        public void Start()
        {
            if (Started)
                throw new System.InvalidOperationException("StopWatch was started twice. This is wrong!");


            StartTime = DateTime.Now;
            Started = true;

        }


        public void Stop()
        {
            Started = false;
            TimeSpan = DateTime.Now - StartTime;
        }

    }
}