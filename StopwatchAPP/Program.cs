﻿using System;
using System.Threading;

namespace StopwatchAPP
{
    class Program
    {
        static void Main(string[] args)
        {

            var sw = new StopWatch();

            sw.Start();
            Thread.Sleep(1000);
            sw.Stop();

            Console.WriteLine("Duration: {0}.", sw.TimeSpan);
        }
    }
}
